module.exports = {
  stories: ['../src/**/*.stories.ts'],
  addons: ['@storybook/addon-actions', '@storybook/addon-links', '@storybook/addon-notes','@storybook/addon-storysource',
  '@storybook/addon-a11y' , '@storybook/addon-knobs'
]
};
