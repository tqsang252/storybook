import { SigninComponent } from "../app/login/signin.component";
import { storiesOf, moduleMetadata } from "@storybook/angular";

import { withA11y } from "@storybook/addon-a11y";

import { boolean, number, text, withKnobs } from "@storybook/addon-knobs";
import { Button } from "@storybook/angular/demo";
import { action } from "@storybook/addon-actions";

//@ts-ignore
import markdownNotes from "../app/login/signin.component.md";
import copyCodeBlock from "@pickra/copy-code-block";

import { HttpClientModule } from "@angular/common/http";

storiesOf("Login Component", module)
  .addDecorator(withA11y)
  .addDecorator(withKnobs)
  .addDecorator(
    moduleMetadata({
      imports: [HttpClientModule],
      declarations: [SigninComponent],
      providers: [] //Service
    })
  )
  .add(
    "fullLoginForm",
    () => {
      return {
        template: `<app-signin></app-signin>`,
        component: Button,
        props: {}
      };
    },
    {
      notes: { markdown: markdownNotes }
    }
  )
  .add("SourceCode", () => {
    return {
      template: `${copyCodeBlock(`
          <div class="signin-component">
              <div class="form-login">
                <div class="form-header">
                    <p class="form-title">Wellcome</p>
                    <img class = "form-logo" src="assets/images/logo-Ari.png" alt="">
                </div>
                <div class="form-content">
                    <form>
                        <input type="text" placeholder="Email">
                        <input type="password" placeholder="Password">
                        <button class=" btn btn-primary">Login</button>
                    </form>
                    <p>Don’t have an account? <a routerLink="user/signup">Sign Up</a></p>
                    <p><a href="#">Forgot password</a></p>
                </div>
              </div>
          </div>

        `)}`,
      component: Button,
      props: {}
    }
  })
  .add(
    "Test",
    () => {
      return {
        template: `<app-signin></app-signin>`,
        component: Button,
        props: {}
      };
    })
